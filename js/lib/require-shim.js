﻿var require = {
    shim: {
        "bootstrap": {
            "deps": ["jquery"]
        }
    },
    paths: {
        "jquery": "../js/lib/jquery",
        "bootstrap": "../js/lib/bootstrap",
        "bootstrap3typeahead": "../js/lib/bootstrap3typeahead"
    }
};