require(["jquery", "bootstrap", "bootstrap3typeahead", "app/app"], function($, b, b3ta, App) {
	
	document.addEventListener('deviceready', function() {
		var app = new App();
		app.init();
	}, false);	
});