define(["jquery"], function($) {
	
	
	/**
	 * @param {Object} params
	 * @param {Array} dataArray
	 */
	function TimedXHRChain(params, dataArray) {
		this._requests = params.requests;
		this._offset = params.offset;
		this._callback = params.callback;
		this._errback = params.errback;
		this._data = dataArray;
		
	}
	
	
	TimedXHRChain.prototype.execute = function(callback, errback) {
				
		$.each(this._requests, function(i, item) {
			var ms = this._offset * i;
			setTimeout(function(){
				$(this._requests[i]).on("request-success", function(evt, resp){
					callback(evt, resp, i);
				});
				$(this._requests[i]).on("request-error", function(evt, err){
					errback(evt, err, i);
				});
				this._requests[i].request();
			}.bind(this), this._offset * (i + 1));	
		}.bind(this));			
	}
	
	TimedXHRChain.prototype.__stub = null;
	return TimedXHRChain;
});