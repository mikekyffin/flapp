define(["jquery", "app/config/cartodb"], function($, config){
	
	/**
	 * 
	 * @param query
	 * @param callback
	 * @param errback
	 * @param allback
	 * @returns
	 */
	function CartoDBQuery(query, callback, errback, allback){
		
		this._query = query;		
		if (callback) {
			this._callback = callback;
		}
		
		if (errback) { 
			this._errback = errback;
		}
		
		if (allback) {
			this._allback = allback;
		}	
		
		return this;
	}
	
	/**
	 * 
	 * 
	 */
	CartoDBQuery.prototype.request = function() {
		
		$(this).trigger("request-start");
		var xhr = $.getJSON(config.sql_api +  "?" + $.param({
			"q": this._query,
			"api_key": config.api_key
		}));
		
		xhr.done(function(response){
			$(this).trigger("request-success", response);
			if (this._callback) callback();
		}.bind(this));
		
		xhr.fail(function(errer){
			$(this).trigger("request-error", error);
			if (this._errback) errback();
		}.bind(this));
		
		xhr.always(function(){
			$(this).trigger("request-complete");
			if (this._allback) allback();
		}.bind(this));
	}
	
	return CartoDBQuery;
	
});