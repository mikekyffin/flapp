
define([], function(){

	/**
	 * 
	 * @contsrcutor
	 * @param {HTMLElement} parentNode
	 * @param {String} text
	 * @param {Integer|Float} value
	 */
	function StatView(parentNode, text, value) {
		this._parent = parentNode;
		this._node = $("<div>", {
			"class": "stat-group"
		});
		
		this._value = $("<div>", {
			"html": value,
			"class": "stat-value"
		}).appendTo(this._node);
		
		this._label = $("<div>", {
			"html": $("<span>", {
				"html": text,
				"class": "stat-text"
			}),
			"class": "stat-label"
		}).appendTo(this._node);
		

		
		
	} 
	
	
	/**
	 * 
	 */
	StatView.prototype.render = function() {
		this._parent.append(this._node);
	}
	
	return StatView;
});