define([
	"jquery", 
	"app/mvc/collection/Collection",
	"app/xhr/CartoDBQuery",
	"app/xhr/TimedXHRChain",
	"app/ui/StatView"
], function($, Collection, CartoDBQuery, TimedXHRChain, StatView){
	
	//SELECT COUNT(DISTINCT(com_name)), extract(MONTH from timestamp) as month FROM sighting_log WHERE EXTRACT(YEAR FROM timestamp) = 2014 GROUP BY month ORDER BY month ASC
	
	var queries = [
	               {
	            	   "title": "Species in 2015",
         			   "query": "SELECT COUNT(DISTINCT(com_name))FROM sighting_log WHERE EXTRACT(YEAR FROM timestamp) = 2015"
	               },
	               {
	            	   "title": "Species in 2014",
	            	   "query": "SELECT COUNT(DISTINCT(com_name)) FROM sighting_log WHERE EXTRACT(YEAR FROM timestamp) = 2014"
	               },
	               {
	            	   "title": "Species in 2013",
	            	   "query": "SELECT COUNT(DISTINCT(com_name)) FROM sighting_log WHERE EXTRACT(YEAR FROM timestamp) = 2013"
	               }      			
    ];
	/**
	 * 
	 * @constructor
	 */
	function Stats(node){
		this._node = node;
		
		this.collection = new Collection();
		this.collection.setItems(queries);
		
		


						
		var chain = new TimedXHRChain({
			requests: $.map(this.collection.getItems(), function(item, i) {
				return new CartoDBQuery(item.query);
			}),
			offset: 500
		});
		
		chain.execute(this._onChainQueryComplete.bind(this), this._onChainQueryError.bind(this));
		
	}
	
	
	/**
	 * 
	 * 
	 */
	Stats.prototype._onChainQueryComplete = function(evt, response, i) {
		var text = this.collection.get(i).title;
		var value = response.rows[0].count;
		var view = new StatView(this._node, text, value);
		view.render();
	}
	
	
	/**
	 * 
	 * 
	 */
	Stats.prototype._onChainQueryError = function(evt, error, i) {
		console.log(error);
	}
	
	Stats.prototype.__stub = null;
	
	Stats.prototype.getTotalLogs = function() {
		
		var query = "SELECT COUNT(*) from sighting_log";	
		var req = new CartoDBQuery(query);		
		$(req).on("request-success", function(evt, response){
			var val = response.rows[0].count
			var report = $("<div>").html("Total number of logs: " + val);
			$(this._node).append(report);
		}.bind(this));		
		$(req).on("request-error", function(evt, error){
			//
		}.bind(this));		
		req.request();
	}
	
	
	Stats.prototype.getTotalBySpecies = function() {
		
		var query = "SELECT com_name, COUNT(*) AS count FROM sighting_log GROUP BY com_name ORDER BY count DESC";	
		var req = new CartoDBQuery(query);		
		$(req).on("request-success", function(evt, response){
			var val = response.rows[0].count
			var report = $("<div>");
			for (var i = 0; i < response.rows.length; i++) {
				var row = $("<div>")
					.html(response.rows[i].com_name + ": " + response.rows[i].count)
					.appendTo(report);
			}
			$(this._node).append(report);
		}.bind(this));		
		$(req).on("request-error", function(evt, error){
			//
		}.bind(this));		
		req.request();
	}
	
	
	
	
	// return for require.js
	return Stats;
	
});