/**
 * 
 * 
 */
define(["jquery"], function($){
	
	var MSG_TYPE = {
			"MESSAGE-ERROR-ICON": "fa fa-exclamation-triangle error",
			"MESSAGE-SUCCESS-ICON": "fa fa-check-circle success",
			"MESSAGE-WARNING-ICON": "fa fa-info-circle warning",
			"MESSAGE-INFO-ICON": "fa fa-info-circle info",
			"MESSAGE-SPIN-ICON": "fa fa-spinner fa-spin",
			"MESSAGE-DEFAULT-ICON": ""
	}
	
	/**
	 * 
	 * @constructor
	 * @param {Object} params
	 */
	function Message(params) {
		
		this._params = params;
		
		// UI elements
		this._node = $("<div>", {
			"class": (params.guid) ? params.guid : ""
		});
		this._node.addClass("log-message");
		this._icon = $("<i>").appendTo(this._node);
		this._msg = $("<span>").appendTo(this._node);
		
		// determine the message type
		var icon = (this._params.type) ? this._params.type : "DEFAULT";
		var type = this.getTypeClass(icon);
		
		//set the icon and message
		$(this._icon).addClass(type);
		$(this._msg).html(this._params.message);
		return this;
	}
		
	
	/**
	 * @param {String} type
	 */
	Message.prototype.getTypeClass = function(type){		
		return MSG_TYPE["MESSAGE-" + type.toUpperCase() + "-ICON"];
	}
	
	
	/**
	 * 
	 * @param {HTMLElement} domNode
	 */
	Message.prototype.appendTo = function(domNode) {
		this._node.appendTo(domNode);
	}
	
	
	/**
	 * 
	 * @param {HTMLElement} domNode
	 * @param {String} type - the type of message to display: Success, Warning, Info, Error, etc
	 */
	Message.prototype.renderTo = function(domNode) {
		var fadeMs  = (this._params.fade) ? this._params.fade : 5000;
		this.appendTo(domNode);	
		this._node.fadeOut(fadeMs, function() {
			this._node.remove();
		}.bind(this));
	}
	
	
	return Message;
});