define([], function(){
	
	/**
	 * @param {Array} fields
	 */
	function Model(fields) {
		this._fields = {};
	}
	
	
	/**
	 * 
	 */
	Model.prototype.toJson = function() {
		return JSON.stringify(this._fields);
	}
	
	
	/**
	 * 
	 */
	Model.prototype.getFields = function() {
		return this._fields;
	}
	
	
	/**
	 * 
	 */
	Model.prototype.setFields = function(fields) {
		this._fields = fields;
	}
	
	
	/**
	 * 
	 */
	Model.prototype.setField = function(fieldName, value) {
		this._fields["fieldName"] = value;
	}
	
	
	/**
	 * 
	 */
	Model.prototype.getField = function(fieldName) {
		return this._fields["fieldName"];
	}
	
	
	return Model;	
});