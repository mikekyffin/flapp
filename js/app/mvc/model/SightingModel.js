

define(["app/mvc/model/Model"], function(Model) {
	
	function SightingModel() {
		Model.call(this);
	}
	SightingModel.prototype = Object.create(Model.prototype);
	
	return SightingModel;
});