/**
 * 
 * 
 */
define([
	"jquery", 
	"app/mvc/collection/Collection",
	"app/xhr/CartoDBQuery"
], function($, Collection, CartoDBQuery){

	/**
	 * 
	 * @constructor 
	 * @param
	 * @param
	 * @param
	 */
	function AsyncCollection(query, callback, errback) {
		this._query = query;
		this._callback = (callback) ? callback : null;
		this._errback = (errback) ? errback : null;
		Collection.call(this);		
	}	
	AsyncCollection.prototype =  Object.create(Collection.prototype);

	/**
	 * 
	 * 
	 */
	AsyncCollection.prototype.getAsync = function() {
		
		if (!this._query) throw "AsyncCollection error: no query supplied."
		
		$(this).trigger("async-start");						
		var req = new CartoDBQuery(this._query);			
		$(req).on("request-success", function(evt, response){
			this.setItems(response.rows);
			$(this).trigger("async-success", response);
			if (this._callback) this._callback();
		}.bind(this));				
		$(req).on("request-error", function(evt, error){
			throw "AsyncCollection error: " + error.statusText;
			$(this).trigger("async-error", error);
			 if (this._errback) this._errback()
		});		
		req.request();
	}
	
	return AsyncCollection;
});
