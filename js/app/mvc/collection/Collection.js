define(["jquery"], function($){
	
	function Collection() {
		this._items = [];						
	}
	
	Collection.prototype
	/**
	 * 
	 */
	Collection.prototype.get = function(index) {
		return this._items[index];
	}
	
	/**
	 * 
	 */
	Collection.prototype.setItems = function(array) {
		this._items = array;
	}
	
	
	/**
	 * 
	 */
	Collection.prototype.getItems = function() {
		return this._items;
	}
	
	
	/**
	 * 
	 */
	Collection.prototype.add = function(item) {
		this._items.push(item);
	}
	
	/**
	 * 
	 */
	Collection.prototype.getItemsByField = function(name) {
		return $.map(this._items, function(obj, i) {
			return obj[name];
		});
	}
	
	
	/**
	 * 
	 */
	Collection.prototype.toJson = function() {
		return JSON.stringify(this._items);
	}
	
	
	/**
	 * 
	 */
	Collection.prototype.reverse = function() {
		this._items.reverse();
	}
	
	return Collection;
});