define(["jquery"], function($) {
	
	function Router(){
		this._anchors = [];
		this._pages = [];
		this._routes = [];
	}
	
	
	/**
	 * 
	 */
	Router.prototype.initialize = function() {
		
		this._anchors =  $(".navr-item");
		
		for (var i = 0; i < this._anchors.length; i++) {

			var anchor = this._anchors[i];
			var page = $("[data-page-container='" + $(this._anchors[i]).data("page-target") + "']");								
			var route = "#!/" + $(this._anchors[i]).data("page-target");
			
			// store the pages & routes in indices that match the anchors (by order)
			this._pages.push(page);
			this._routes.push(route);	
			
			$(anchor).on("click", function(evt) {
				var pageName = $(evt.target).parent().data("page-target");
				$("[data-page-container]").css("display", "none");	
				$("[data-page-container='" + pageName  + "']").fadeIn(222);		
			});
		}

		return this;
	}
	
	/**
	 * 
	 */
	Router.prototype.getRoutes = function() {
		return this._routes;
	}
	
	Router.prototype.setRouteVisible = function(route) {
		var page = $("[data-page-container='" + route + "']");
		page.css("display", "block");
	}
	
	
	return Router;
});