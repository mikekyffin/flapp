/**
 * 
 * 
 * 
 * 
 */
define([
	"app/mvc/Router", 
	"app/mvc/collection/AsyncCollection", 
	"app/mvc/collection/LocalStorageSightingCollection",
	"app/xhr/CartoDBQuery",
	"app/config/cartodb",
	"app/ui/Stats",
	"app/ui/Message"
], function(
	Router, 
	AsyncCollection, 
	LocalStorageSightingCollection, 
	CartoDBQuery, 
	config, 
	Stats, 
	Message
) {

	/* start: closured stuff */
	var messaging = {
		"message-error": "",
		"message-success": ""
	};
	var LOCATION_TRACKING_REFRESH_MS = 20000;
	/* end: closured stuff */
	
	/**
	 * 
	 */
	function App(){
		this.positionID = null;
		this.com_names = [];
		this["flapp-daily-db"] = new LocalStorageSightingCollection();
		

	} 
	
	App.prototype.__stub = null;

	/**
	 * 
	 * @params {Object} params
	 */
	App.prototype._onDuplicate = function(params) {
		this.message("add-bird", params.com_name + " is already in the database.", "warning");
	}
	
	/**
	 * @param {Object} params - a set of query parameters 
	 */
	App.prototype._onNoDuplicate = function(params) {

		var query = "INSERT INTO species_list (com_name, sci_name, uuid) VALUES (%-1-, %-2-, %-3-)"
			.replace("%-1-", "'" + params.com_name.replace("'", "''") + "'")
			.replace("%-2-", "'" + params.sci_name + "'")
			.replace("%-3-", "'" + params.uuid + "'");		
		
		
		var req = new CartoDBQuery(query);		
		$(req).on("request-success", function(evt, response){
			this._onSubmitSpeciesSuccess();
		}.bind(this));		
		$(req).on("request-error", function(evt, error){
			this._onSubmitSpeciesError();
		});		
		req.request();
	}
	
	
	/**
	 * 
	 * 
	 */
	App.prototype._onSubmitSpeciesSuccess = function() {
		$("#insert-bird-com-name").val(""),
		$("#insert-bird-sci-name").val(""),
		this.message("add-bird", "Added 1 new item to database.", "success");
	}
	
	
	/**
	 *
	 * 
	 */
	App.prototype._onSubmitSpeciesError = function() { 
		this.message("add-bird", "Error: Cannot connect to database", "error");
	}
	
	
	/**
	 * 
	 * @params {Object} params
	 * @params {Function} callback
	 * @params {Function} dupeback
	 * @params {Function} errback
	 */
	App.prototype._checkForDuplicates = function(params, callback, dupeback, errback) {

		
		this.message("add-bird", "Checking for duplicate...", "info");		
		var query =  "SELECT com_name FROM species_list WHERE com_name = %-1-".replace("%-1-", "'" + params.com_name.replace("'", "''") + "'");
		var req = new CartoDBQuery(query);		
		$(req).on("request-success", function(evt, response){
			if (response.rows.length > 0) {
				dupeback();
			}  else {
				callback();
			}
		}.bind(this));		
		$(req).on("request-error", function(evt, error){
			errback();
		});		
		req.request();		
	}
	
	
	/**
	 * 
	 * @param {Object} response
	 */
	App.prototype._onSubmitSuccess = function(response, sighting) {
		
		$("#select-bird").val("");	
		
		if ($("#use-current-timestamp").prop("checked")) {
			$("#select-timestamp").val("auto")
		} 
		
		if ($("#use-current-location").prop("checked")) {
			//this.addLocation();
		} 

		var msg = "Added 1 '" + sighting.com_name + "' sighting to log";
		this.message("log-sighting", msg, "success");
		
		if (this.com_names.indexOf(sighting.com_name) == -1) {
			console.log(sighting.com_name, "new");
		}		
	}
	
	
	/**
	 * 
	 * 
	 */
//	App.prototype._addToSpeciesList = function(com_name) {
//		setTimeout(function(){
//			
//			var query =  "INSERT ");
//			var req = new CartoDBQuery(query);		
//			$(req).on("request-success", function(evt, response){
//				if (response.rows.length > 0) {
//					dupeback();
//				}  else {
//					callback();
//				}
//			}.bind(this));		
//			$(req).on("request-error", function(evt, error){
//				errback();
//			});		
//			req.request();	
//		}.bind(this), 200);
//	}
	
	
	/**
	 * 
	 * 
	 */
	App.prototype._onSubmitError = function() {
		this.message("log-sighting", "Error: cannot add sighting to log.", "error");
	}
	
	
	/**
	 * 
	 * 
	 */
	App.prototype._initAutocomplete = function() {
		
		//var query = "SELECT DISTINCT com_name FROM sighting_log";	
		var query = "SELECT DISTINCT com_name, COUNT(*) as count FROM sighting_log GROUP BY com_name ORDER BY count DESC";
		var coll = new AsyncCollection(query);	
		$(coll).on("async-success", function(resp) {
			var names = coll.getItemsByField("com_name");
			this._updateAutocomplete(names);
			this.message("log-sighting", "Attached species list from database.", "info");
		}.bind(this));		
		$(coll).on("async-error", function(err) {
			// this.message("log-sighting", "<i class=\"fa fa-exclamation-circle error\"></i> Error: cannot attach species list to input form.");
			// grab the previous list from local storage and use that
			this._updateAutocomplete(localStorage.getItem("flapp-species"));
			this.message("log-sighting", "Attached species list from local storage.", "info");
		}.bind(this));
		coll.getAsync();			
	}
	
	
	/**
	 * Attach or re-attach an array to the typeahead element
	 * @param {Array} com_names - and array of common names
	 */
	App.prototype._updateAutocomplete = function(com_names) {
		$("#select-bird").typeahead({ 
			"source": com_names,
			"items": 10
		});		
		// this might be redundant is we're using local storage - but what if it isn't working or
		// otherwise unavailable in the WebView?
		this.com_names = com_names;				
		// let's keep a copy in local storage so we can search it if the query to CartoDB fails on start-up (which 
		// it does sometimes in areas of low connectivity)
		if(localStorage) {
			localStorage.setItem("flapp-species", com_names);
		} else {
		    // no web storage
		}
	}
	
		
	
	/**
	 * 
	 * @param {String} container - the data-page-container target id
	 * @param {String} message - the message (text or HTML) to be displayed to the user
	 */
	App.prototype.message = function(container, message, type, guid) {
		
		var cont = $(".page-messaging", $("[data-page-container='" + container + "']"));		
		new Message({
			"type": type,
			"message": message,
			"fade": 10000,
			"guid": guid
		}).renderTo(cont);
	}
	
	
	/**
	 * 
	 * 
	 */
	App.prototype.submitSpecies = function() {
			
		var species = {
			"com_name": $("#insert-bird-com-name").val(),
			"sci_name": $("#insert-bird-sci-name").val(),
			"uuid": this.guid()
		}
	
		this._checkForDuplicates(
				species, 
				this._onNoDuplicate.bind(this, species), 
				this._onDuplicate.bind(this, species), 
				function(){}
		);
		
	}
				
	/**
	 * 
	 */
	App.prototype.init = function() {
				
		var router = new Router();
		router.initialize();
		router.setRouteVisible("log-sighting");
		
		
		
		$("#submit-sighting").on("click", function(){
			this.submitSighting();
		}.bind(this));
		
		$("#submit-species").on("click", function(){
			this.submitSpecies();
		}.bind(this));
		
		$("#location-test").on("click", function(evt){
			this.addLocation(evt);
		}.bind(this));
		
		$("#use-current-location").on("change", function(evt){
			if ($(evt.target).prop("checked")) {
				this.startLocationTracking();
				$("#select-lat").attr('disabled', true);
				$("#select-lng").attr('disabled', true);

			} else {
				$("#select-lat").val("").attr('disabled', false);
				$("#select-lng").val("").attr('disabled', false);
				this.stopLocationTracking();
			}
		}.bind(this));
		
		$("#select-timestamp").val("auto");
		$("#use-current-timestamp").on("change", function(evt){
			if ($(evt.target).prop("checked")) {
				$("#select-timestamp").val("auto").attr('disabled', true);
			} else {
				$("#select-timestamp").val("").attr('disabled', false);				
			}
		}.bind(this));
		
		this.startLocationTracking();
		this._initAutocomplete();	
		
		document.addEventListener("pause", this._onPause.bind(this), false);
		
	    document.addEventListener("resume", this._onResume.bind(this), false);

	    var stats = new Stats($("[data-page-container='stats']"));
	    
		// check local storage for daily tracking
		var date = new Date();
		
		if (localStorage && localStorage.getItem("flapp-daily-db")) {
			var dbDay = this["flapp-daily-db"].getDay();
			console.log(dbDay);
			if (date.getDay() != dbDay) {
				this["flapp-daily-db"].setEmpty({
					"_day": date.getDay(),
					"_items": []
				});
			}	
		} else {
			this["flapp-daily-db"].setEmpty({
				"_day": date.getDay(),
				"_items": []
			});
		}
		
		this["flapp-daily-db"].add({
			"timestamp": "now",
			"what": "cheese",
			"guid": "asdfasg34634634635"
		});
		
//		if (localStorage && localStorage.getItem("flapp-daily-db")) {
//			if (date.getDay < 7 && localStorage.getItem("flapp-daily-db").getDayAsInteger() > date.getDay()) {
//				localStorage.setItem("flapp-daily-db", new LocalStorageSightingCollection(date.getDay()));
//			} else {
//				this.message("log-sighting", "Connecting to local db for " + localStorage.getItem("flapp-daily-db").getDayAsText(), "info", null);
//			}
//			
//		} else if (localStorage && !localStorage.getItem("flapp-daily-db")) {
//			localStorage.setItem("flapp-daily-db", new LocalStorageSightingCollection(date.getDay()));
//		}
	}
	
	App.prototype._onPause = function() {
		this.message("log-sighting", "Pausing...", "info", null);
		this.stopLocationTracking();
	}
	
	App.prototype._onResume = function() {
		
		// friendly message
		this.message("log-sighting", "Resuming...", "info", null);		
    	if ($("#use-current-location").prop("checked")) {	    		
			this.startLocationTracking();
		} 
	}
	
	
	/**
	 * 
	 * 
	 */
	App.prototype.startLocationTracking = function() {
		
		this.watchPosition();
		this.message("log-sighting", "Resuming position tracking...", "info", null);	
	}
	
	
	/**
	 * 
	 */
	App.prototype.stopLocationTracking = function() {
		navigator.geolocation.clearWatch(this.watchID);
	}
	
	
	App.prototype.watchPosition = function() {
		var options = { 
			enableHighAccuracy: true, 
			timeout: 60000  
		};
		var id = navigator.geolocation.watchPosition(this._mobileLocationSuccess.bind(this), this._mobileLocationError.bind(this), options);
		this.positionID = id;
	}
	
	
	App.prototype._mobileLocationSuccess = function(position) {
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
        $("#select-lat").val(lat);
        $("#select-lng").val(lng);
        //this.message("log-sighting", "Location updated!", "info");
	}
	
	App.prototype._mobileLocationError = function(error) {
		console.log("error: problem getting position");
		console.log(error);
  	  	this.message("log-sighting", "Error: Cannot retrieve location!", "error");
	}
	
	/**
	 * 
	 */
	App.prototype.submitSighting = function() {
		
		var timestamp;
		var guid = this.guid();
		this.message("log-sighting", "Writing to log...", "info", guid);
		
		if ($("#use-current-timestamp").prop("checked")) {
			timestamp = new Date().toISOString();
		} else {
			timestamp = this.dateToISOString($("#select-timestamp").val());
		}	
		
		var sighting = {
			"com_name": $("#select-bird").val(),
			"lat": $("#select-lat").val(),
			"lng": $("#select-lng").val(),
			"timestamp": timestamp
		}
		
		if (!$("#use-current-timestamp").prop("checked")) {
			this.dateToISOString($("#select-timestamp").val());
		}
		
		var query = "INSERT INTO sighting_log (com_name, sci_name, lat, lng, the_geom, timestamp) VALUES (%-1-, (SELECT sci_name FROM species_list WHERE com_name = %-6-), %-2-, %-3-, %-4-, %-5-)"
			.replace("%-1-", "'" + sighting.com_name.replace("'", "''") + "'")
			.replace("%-6-", "'" + sighting.com_name.replace("'", "''") + "'")
			.replace("%-2-", sighting.lat)
			.replace("%-3-", sighting.lng)
			.replace("%-4-", "ST_PointFromText('POINT(" + sighting.lng + " " + sighting.lat + ")', 4326)")
			.replace("%-5-", "'" + sighting.timestamp + "'");
		
		var req = new CartoDBQuery(query);		
		$(req).on("request-success", function(evt, response){
			this._onSubmitSuccess(response, sighting);
			$("." + guid).remove();
		}.bind(this));		
		$(req).on("request-error", function(evt, error){
			this._onSubmitSuccess(error);
		});		
		req.request();
	};
	
	/**
	 * 
	 * TODO: date validation!
	 * @param {String} inputDate
	 */
	App.prototype.dateToISOString = function(inputDate) {
		
		var dateElems = inputDate.split("-");
		var date = new Date(dateElems[0], dateElems[1], dateElems[2]);
		return date.toISOString();
	}	
	
	/**
	 * @returns
	 */
	App.prototype.guid = function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);		      
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	
	/**
	 * @param {Event} event
	 * @returns
	 */
	App.prototype.addLocation = function(event) {
		
		if (event) event.preventDefault();
		this.message("log-sighting", "Refreshing location...", "info");
		navigator.geolocation.getCurrentPosition(
			function(position) {
				var lat = position.coords.latitude;
				var lng = position.coords.longitude;
		        $("#select-lat").val(lat);
		        $("#select-lng").val(lng);
		        console.log("info: set position");
		        //this.message("log-sighting", "Location set!", "success");
		      }.bind(this),
		      function() {
		    	  console.log("error: problem getting position");
		    	  this.message("log-sighting", "Error: Cannot retrieve location!", "error");
		      }.bind(this));
		return false;
	};
		
	/**
	 * @param {Event} event
	 */
	App.prototype.addTimestamp = function(event) {
		$("#select-timestamp").val(new Date().toISOString());
	};
		
	// return the whole object to require.js
	return App;
});
